/* TODO: fos js routes */
/* TODO: beauty notifications :) */
/* TODO: Loading animation */

$(document).ready(function() {
    $('a.js-send-email').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/mail/best_markets',
            success: function (response) {
                status = response.value;

                if (status) {
                    alert('Email was successfully sent');
                } else {
                    alert('Email was not sent');
                }
            },
            error: function () {
                alert('Email was not sent');
            }
        });
    });

    $('a.js-fetch-markets').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            type: 'GET',
            url: '/financial/fetch_markets',
            success: function (response) {
                status = response.value;

                if (status) {
                    alert('Successfully fetched '+response.count+' markets from Google');
                } else {
                    alert('Fetching was failed');
                }
            },
            error: function () {
                alert('Fetching was failed');
            }
        });
    });

    $('a.js-fetch-markets-ohlc').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            type: 'GET',
            url: '/financial/fetch_markets_ohlc',
            success: function (response) {
                status = response.value;

                if (status) {
                    alert('Successfully fetched OHLC for all markets from Google');
                } else {
                    alert('Fetching was failed');
                }
            },
            error: function () {
                alert('Fetching was failed');
            }
        });
    });
});
