<?php

namespace AppBundle\Browser;

use JonnyW\PhantomJs\Client;
use JonnyW\PhantomJs\Http\{Request, Response};
use Psr\Log\LoggerInterface;

class HttpBrowser implements BrowserInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->setClient(Client::getInstance());
        $this->setLogger($logger);
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $path
     *
     * @return string
     *
     * @throws \Exception
     */
    public function fetch(string $path): string
    {
        $this->logger->info('Start fetching url: '.$path);

        $this->client->isLazy();

        /** @var Request $request */
        $request = $this->client->getMessageFactory()->createRequest($path, Request::METHOD_GET);
        $request->addSetting('userAgent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');

        /** @var Response $response */
        $response = $this->client->getMessageFactory()->createResponse();

        $this->client->send($request, $response);

        // Following redirects in recursion.
        if ($response->isRedirect()) {
            $this->logger->info('Redirected to: '.$response->getRedirectUrl());

            return $this->fetch($response->getRedirectUrl());
        }

        $this->logger->info('Returned response code is '.$response->getStatus());

        // TODO: Add exceptions handling.
        if ($response->getStatus() === 200) {
            return $response->getContent();
        } else {
            $this->logger->error('Something was wrong. Raw content: '.$response->getContent());

            throw new \Exception('Exception');
        }
    }
}
