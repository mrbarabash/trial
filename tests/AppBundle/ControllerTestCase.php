<?php

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Client;

class ControllerTestCase extends AppTestCase
{
    /**
     * @var Client
     */
    protected $client = null;

    public function setUp()
    {
        parent::setUp();

        $this->client = $this->getContainer()->get('test.client');
    }
}
