<?php

namespace AppBundle\Tests;

use AppBundle\Entity\FinancialMarket;
use AppBundle\Entity\FinancialMarketPrice;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class FinancialMarketTest extends TestCase
{
    /**
     * @group unit
     */
    public function testGetPricesForDateMethod()
    {
        $financialMarket = new FinancialMarket();

        $financialMarketPrices = [];
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt(new \DateTime('-2 days'))
            ->setPrice(10)
        ;
        $financialMarketPrices[] = $financialMarketPrice;
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt(new \DateTime('-2 days'))
            ->setPrice(10)
        ;
        $financialMarketPrices[] = $financialMarketPrice;
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt()
            ->setPrice(100)
        ;
        $financialMarketPrices[] = $financialMarketPrice;
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt(new \DateTime('+1 day'))
            ->setPrice(200)
        ;
        $financialMarketPrices[] = $financialMarketPrice;

        $financialMarket->setPrices(new ArrayCollection($financialMarketPrices));

        // Test today: only one price with 100 value.
        /** @var ArrayCollection $datePrices */
        $datePrices = $financialMarket->getPricesForDate();

        static::assertEquals(1, $datePrices->count());
        static::assertEquals(100, $datePrices->first()->getPrice());

        // Test 2 days ago: 2 prices with 10 value.
        /** @var ArrayCollection $datePrices */
        $datePrices = $financialMarket->getPricesForDate(new \DateTime('-2 days'));

        static::assertEquals(2, $datePrices->count());
        foreach ($datePrices->toArray() as $datePrice) {
            static::assertEquals(10, $datePrice->getPrice());
        }

        // Test tomorrow: only one price with 200 value.
        /** @var ArrayCollection $datePrices */
        $datePrices = $financialMarket->getPricesForDate(new \DateTime('+1 day'));

        static::assertEquals(1, $datePrices->count());
        static::assertEquals(200, $datePrices->first()->getPrice());
    }

    /**
     * @group unit
     */
    public function testGetPriceWithMaxChangeMethod()
    {
        $financialMarket = new FinancialMarket();

        $financialMarketPrices = [];
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt()
            ->setPrice(10)
            ->setChange(0.5)
        ;
        $financialMarketPrices[] = $financialMarketPrice;
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt()
            ->setPrice(50)
            ->setChange(-0.27)
        ;
        $financialMarketPrices[] = $financialMarketPrice;
        $financialMarketPrice = (new FinancialMarketPrice())
            ->setCreatedAt()
            ->setPrice(500)
            ->setChange(12.4)
        ;
        $financialMarketPrices[] = $financialMarketPrice;

        $financialMarket->setPrices(new ArrayCollection($financialMarketPrices));

        $maxChangePrice = $financialMarket->getPriceWithMaxChange();

        static::assertNotNull($maxChangePrice);
        static::assertEquals(500, $maxChangePrice->getPrice());
    }
}
