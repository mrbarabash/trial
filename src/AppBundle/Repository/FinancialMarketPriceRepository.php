<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FinancialMarket;
use AppBundle\Entity\FinancialMarketPrice;

class FinancialMarketPriceRepository extends AbstractEntityRepository
{
    /**
     * @param FinancialMarket   $market
     * @param float             $price
     * @param float             $change
     *
     * @return FinancialMarketPrice
     */
    public function create(FinancialMarket $market, float $price, float $change): FinancialMarketPrice
    {
        return (new FinancialMarketPrice())
            ->setPrice($price)
            ->setChange($change)
            ->setFinancialMarket($market)
        ;
    }
}
