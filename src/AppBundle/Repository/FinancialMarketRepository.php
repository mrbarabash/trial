<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FinancialMarket;

class FinancialMarketRepository extends AbstractEntityRepository
{
    /**
     * @param string $name
     * @param string $shortcut
     * @param string $link
     *
     * @return FinancialMarket
     */
    public function createOrUpdate(string $name, string $shortcut, string $link): FinancialMarket
    {
        $financialMarket = $this->findOneBy(['shortcut' => $shortcut]) ?: new FinancialMarket();

        $financialMarket
            ->setName($name)
            ->setShortcut($shortcut)
            ->setDetailsUrl($link)
        ;

        return $financialMarket;
    }

    /**
     * @TODO: Add test
     *
     * @param int $limit
     *
     * @return array
     */
    public function findBestByChangeForToday(int $limit = 5): array
    {
        $today = new \DateTime();

        return $this->createQueryBuilder('fm')
            ->join('fm.prices', 'fmp')
            ->where('fmp.createdAt BETWEEN :from AND :to')
            ->setParameter('from', $today->setTime(0, 0, 0)->format('Y-m-d H:i:s'))
            ->setParameter('to', $today->setTime(23, 59, 59)->format('Y-m-d H:i:s'))
            ->orderBy('fmp.change', 'DESC')
            ->groupBy('fm.id')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }
}
