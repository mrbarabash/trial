<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FinancialMarket;
use AppBundle\Entity\FinancialMarketOHLC;

class FinancialMarketOHLCRepository extends AbstractEntityRepository
{
    /**
     * @param FinancialMarket   $market
     * @param float             $open
     * @param float             $high
     * @param float             $low
     * @param float             $close
     *
     * @return FinancialMarketOHLC
     */
    public function create(FinancialMarket $market, float $open, float $high, float $low, float $close): FinancialMarketOHLC
    {
        return (new FinancialMarketOHLC())
            ->setOpen($open)
            ->setHigh($high)
            ->setLow($low)
            ->setClose($close)
            ->setFinancialMarket($market)
        ;
    }
}
