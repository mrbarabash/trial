<?php

namespace AppBundle\Mail;

use AppBundle\Entity\FinancialMarket;
use AppBundle\Repository\FinancialMarketRepository;
use AppBundle\Serializer\SerializerFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment as Twig;

class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var string
     */
    private $emailFrom;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var FinancialMarketRepository
     */
    private $financialMarketRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param \Swift_Mailer             $mailer
     * @param Twig                      $twig
     * @param TranslatorInterface       $translator
     * @param FinancialMarketRepository $financialMarketRepository
     * @param LoggerInterface           $logger
     */
    public function __construct(\Swift_Mailer $mailer, Twig $twig, ContainerInterface $container, TranslatorInterface $translator, FinancialMarketRepository $financialMarketRepository, LoggerInterface $logger)
    {
        $this->mailer                       = $mailer;
        $this->twig                         = $twig;
        $this->emailFrom                    = $container->getParameter('email_from');
        $this->translator                   = $translator;
        $this->financialMarketRepository    = $financialMarketRepository;
        $this->logger                       = $logger;
    }

    /**
     * @param string $to
     */
    public function sendBestMarketsEmail(string $to)
    {
        $this->logger->info('Start sending email to '.$to);

        /** @var FinancialMarket[] $markets */
        $markets = $this->financialMarketRepository->findAll();
        /** @var FinancialMarket[] $bestMarkets */
        $bestMarkets = $this->financialMarketRepository->findBestByChangeForToday();

        $attachment = \Swift_Attachment::newInstance(
            SerializerFactory::create()->serialize($markets, 'xml'),
            'markets.xml',
            'text/xml'
        );

        $message = (new \Swift_Message($this->translator->trans('emails.best_markets.subject')))
            ->setFrom($this->emailFrom)
            ->setTo($to)
            ->setBody(
                $this->twig->render('emails/best_markets.html.twig', ['markets' => $bestMarkets]),
                'text/html'
            )
            ->attach($attachment)
        ;

        $result = $this->mailer->send($message);

        if ($result == 1) {
            $this->logger->info('Email was successfully sent');
        } else {
            $this->logger->error('Email was not sent');
        }
    }
}
