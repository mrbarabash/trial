<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FinancialMarketRepository")
 * @ORM\HasLifecycleCallbacks
 */
class FinancialMarket
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=50, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(length=20, unique=true)
     */
    private $shortcut;

    /**
     * @var FinancialMarketPrice[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FinancialMarketPrice", mappedBy="financialMarket", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $prices;

    /**
     * @var FinancialMarketOHLC[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="FinancialMarketOHLC", mappedBy="financialMarket", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $ohlcs;

    /**
     * @var string
     *
     * @ORM\Column(length=120)
     */
    private $detailsUrl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->prices   = new ArrayCollection();
        $this->ohlcs    = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return FinancialMarket
     */
    public function setName(string $name): FinancialMarket
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortcut(): string
    {
        return $this->shortcut;
    }

    /**
     * @param string $shortcut
     *
     * @return FinancialMarket
     */
    public function setShortcut(string $shortcut): FinancialMarket
    {
        $this->shortcut = $shortcut;

        return $this;
    }

    /**
     * @return FinancialMarketPrice[]|ArrayCollection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param FinancialMarketPrice[]|ArrayCollection $prices
     *
     * @return FinancialMarket
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;

        return $this;
    }

    /**
     * @param FinancialMarketPrice $price
     *
     * @return FinancialMarket
     */
    public function addPrice(FinancialMarketPrice $price): FinancialMarket
    {
        if (!$this->prices->contains($price)) {
            $this->prices->add($price);
        }

        return $this;
    }

    /**
     * @param FinancialMarketPrice $price
     *
     * @return FinancialMarket
     */
    public function removePrice(FinancialMarketPrice $price): FinancialMarket
    {
        $this->prices->removeElement($price);

        return $this;
    }

    /**
     * @return FinancialMarketPrice
     */
    public function getLastPrice(): FinancialMarketPrice
    {
        return $this->prices->last();
    }

    /**
     * @param \DateTime|null $dateTime
     *
     * @return ArrayCollection|FinancialMarketPrice[]
     */
    public function getPricesForDate(\DateTime $dateTime = null): ArrayCollection
    {
        $dateTime = $dateTime ?: new \DateTime();

        return $this->prices->filter(function (FinancialMarketPrice $price) use ($dateTime) {
            return $price->getCreatedAt()->getTimestamp() > $dateTime->setTime(0, 0, 0)->getTimestamp()
                && $price->getCreatedAt()->getTimestamp() < $dateTime->setTime(23, 59, 59)->getTimestamp();
        });
    }

    /**
     * @param \DateTime|null $dateTime
     *
     * @return FinancialMarketPrice|null
     */
    public function getPriceWithMaxChange(\DateTime $dateTime = null): ?FinancialMarketPrice
    {
        $prices = $this->getPricesForDate($dateTime)->toArray();
        usort($prices, function (FinancialMarketPrice $price1, FinancialMarketPrice $price2) {
            return (float) $price2->getChange() < (float) $price1->getChange() ? -1 : 1;
        });

        return count($prices) ? reset($prices) : null;
    }

    /**
     * @return FinancialMarketOHLC[]|ArrayCollection
     */
    public function getOHLCs()
    {
        return $this->ohlcs;
    }

    /**
     * @param FinancialMarketOHLC[]|ArrayCollection $ohlcs
     *
     * @return FinancialMarket
     */
    public function setOHLCs($ohlcs)
    {
        $this->ohlcs = $ohlcs;

        return $this;
    }

    /**
     * @param FinancialMarketOHLC $ohlc
     *
     * @return FinancialMarket
     */
    public function addOHLC(FinancialMarketOHLC $ohlc): FinancialMarket
    {
        if (!$this->ohlcs->contains($ohlc)) {
            $this->ohlcs->add($ohlc);
        }

        return $this;
    }

    /**
     * @param FinancialMarketOHLC $ohlc
     *
     * @return FinancialMarket
     */
    public function removeOHLC(FinancialMarketOHLC $ohlc): FinancialMarket
    {
        $this->ohlcs->removeElement($ohlc);

        return $this;
    }

    /**
     * @return FinancialMarketOHLC
     */
    public function getLastOHLC(): FinancialMarketOHLC
    {
        return $this->ohlcs->last();
    }

    /**
     * @return string
     */
    public function getDetailsUrl(): string
    {
        return $this->detailsUrl;
    }

    /**
     * @param string $detailsUrl
     *
     * @return FinancialMarket
     */
    public function setDetailsUrl(string $detailsUrl): FinancialMarket
    {
        $this->detailsUrl = $detailsUrl;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
