<?php

namespace AppBundle\Serializer;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerFactory
{
    /**
     * @return Serializer
     */
    public static function create(): Serializer
    {
        $encoder = new XmlEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return (string) $object;
        });

        return new Serializer([$normalizer], [$encoder]);
    }
}
