<?php

namespace Tests\AppBundle\Browser;

use AppBundle\Browser\HttpBrowser;
use JonnyW\PhantomJs\Client;
use JonnyW\PhantomJs\Http\{MessageFactory, Request, Response};
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class HttpBrowserTest extends TestCase
{
    /**
     * @group unit
     */
    public function testFetch()
    {
        $httpBrowser = $this->createHttpBrowserMock();

        static::assertEquals('Test content', $httpBrowser->fetch('https://google.com'));
    }

    /**
     * @expectedException \Exception
     *
     * @group unit
     */
    public function testException()
    {
        $httpBrowser = $this->createHttpBrowserMock(500);

        $httpBrowser->fetch('https://google.com');
    }

    private function createHttpBrowserMock(int $responseStatus = 200)
    {
        $httpBrowser = $this->getMockBuilder(HttpBrowser::class)
            ->disableOriginalConstructor()
            ->setMethodsExcept(['fetch', 'setClient', 'setLogger'])
            ->getMock();

        $messageFactory = $this->getMockBuilder(MessageFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $request = new Request();
        $messageFactory
            ->expects($this->once())
            ->method('createRequest')
            ->willReturn($request);
        $response = new Response();
        $response->status = $responseStatus;
        $response->content = 'Test content';
        $messageFactory
            ->expects($this->once())
            ->method('createResponse')
            ->willReturn($response);

        $client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
        $client
            ->expects($this->exactly(2))
            ->method('getMessageFactory')
            ->willReturn($messageFactory);
        $client
            ->expects($this->once())
            ->method('send')
            ->with($request, $response);

        $logger = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->getMock();

        $httpBrowser->setClient($client);
        $httpBrowser->setLogger($logger);

        return $httpBrowser;
    }
}
