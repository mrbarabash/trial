<?php

namespace Tests\AppBundle;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class AppTestCase extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function setUp()
    {
        parent::setUp();

        static::bootKernel();

        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return Container
     */
    protected function getContainer(): Container
    {
        return static::$kernel->getContainer();
    }
}
