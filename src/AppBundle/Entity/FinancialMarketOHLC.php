<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @TODO: Save only one OHLC value for day (createOrUpdate method).
 *
 * @ORM\Table
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FinancialMarketOHLCRepository")
 * @ORM\HasLifecycleCallbacks
 */
class FinancialMarketOHLC
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var FinancialMarket
     *
     * @ORM\ManyToOne(targetEntity="FinancialMarket", inversedBy="ohlcs")
     */
    private $financialMarket;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $open;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $high;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $low;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $close;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return FinancialMarket
     */
    public function getFinancialMarket(): FinancialMarket
    {
        return $this->financialMarket;
    }

    /**
     * @param FinancialMarket $financialMarket
     *
     * @return FinancialMarketOHLC
     */
    public function setFinancialMarket(FinancialMarket $financialMarket): FinancialMarketOHLC
    {
        $this->financialMarket = $financialMarket;

        return $this;
    }

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     *
     * @return FinancialMarketOHLC
     */
    public function setOpen(float $open): FinancialMarketOHLC
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     *
     * @return FinancialMarketOHLC
     */
    public function setHigh(float $high): FinancialMarketOHLC
    {
        $this->high = $high;

        return $this;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     *
     * @return FinancialMarketOHLC
     */
    public function setLow(float $low): FinancialMarketOHLC
    {
        $this->low = $low;

        return $this;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     *
     * @return FinancialMarketOHLC
     */
    public function setClose(float $close): FinancialMarketOHLC
    {
        $this->close = $close;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s;%s;%s;%s', $this->getOpen(), $this->getHigh(), $this->getLow(), $this->getClose());
    }
}
