<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FinancialMarketPriceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class FinancialMarketPrice
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var FinancialMarket
     *
     * @ORM\ManyToOne(targetEntity="FinancialMarket", inversedBy="prices")
     */
    private $financialMarket;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=6, scale=2)
     */
    private $change;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return FinancialMarket
     */
    public function getFinancialMarket(): FinancialMarket
    {
        return $this->financialMarket;
    }

    /**
     * @param FinancialMarket $financialMarket
     *
     * @return FinancialMarketPrice
     */
    public function setFinancialMarket(FinancialMarket $financialMarket): FinancialMarketPrice
    {
        $this->financialMarket = $financialMarket;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return FinancialMarketPrice
     */
    public function setPrice(float $price): FinancialMarketPrice
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getChange(): float
    {
        return $this->change;
    }

    /**
     * @param float $change
     *
     * @return FinancialMarketPrice
     */
    public function setChange(float $change): FinancialMarketPrice
    {
        $this->change = $change;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * For testing purposes...
     *
     * @internal
     *
     * @param \DateTime|null $createdAt
     *
     * @return FinancialMarketPrice
     */
    public function setCreatedAt(\DateTime $createdAt = null): FinancialMarketPrice
    {
        $this->createdAt = $createdAt ?: new \DateTime();

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getPrice();
    }
}
