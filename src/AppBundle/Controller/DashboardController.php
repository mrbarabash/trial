<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FinancialMarket;
use AppBundle\Repository\FinancialMarketRepository;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends BaseController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function indexAction(FinancialMarketRepository $financialMarketRepository)
    {
        $bestMarkets = $financialMarketRepository->findBestByChangeForToday();

        $series = array_map(function (FinancialMarket $market) {
            return $market->getPriceWithMaxChange()->getChange();
        }, $bestMarkets);
        $categories = array_map(function (FinancialMarket $market) {return (string) $market;}, $bestMarkets);

        return $this->render('dashboard/index.html.twig', ['chart' => $this->createChart($series, $categories)]);
    }

    /**
     * @TODO: Move to separate service.
     *
     * @param array $seriesData
     * @param array $categories
     *
     * @return Highchart
     */
    private function createChart(array $seriesData, array $categories): Highchart
    {
        $series = [
            [
                'name' => 'Change',
                'data' => $seriesData,
            ]
        ];

        $chart = new Highchart();
        $chart->chart->renderTo('linechart');
        $chart->chart->type('bar');
        $chart->title->text('Financial Markets Changes Today');
        $chart->xAxis->categories($categories);
        $chart->yAxis->title(['text'  => 'Change']);
        $chart->series($series);

        return $chart;
    }
}
