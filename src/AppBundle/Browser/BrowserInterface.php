<?php

namespace AppBundle\Browser;

interface BrowserInterface
{
    /**
     * Fetch data from different sources: HTTP, filesystem, etc.
     *
     * @param string $path
     *
     * @return string
     */
    public function fetch(string $path): string;
}
