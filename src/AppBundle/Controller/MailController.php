<?php

namespace AppBundle\Controller;

use AppBundle\Mail\Mailer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MailController extends BaseController
{
    /**
     * @Route("/mail/best_markets", name="mail_best_markets", methods={"POST"})
     */
    public function sendBestMarketsEmailAction(Mailer $mailer)
    {
        // TODO: Add form to input the receiver's email address.
        $mailer->sendBestMarketsEmail('eyyub@learn-solve.com');

        return new JsonResponse(['success' => true]);
    }
}
