<?php

namespace AppBundle\Manager;

use AppBundle\Browser\HttpBrowser;
use AppBundle\Entity\FinancialMarket;
use AppBundle\Entity\FinancialMarketOHLC;
use AppBundle\Repository\FinancialMarketOHLCRepository;
use AppBundle\Repository\FinancialMarketPriceRepository;
use AppBundle\Repository\FinancialMarketRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\TranslatorInterface;

class GoogleFinanceManager
{
    const WORLD_MARKETS_XPATH = '(//div[@class="rit-block"]//*//div[contains(@class, "vnLNtd")])[2]//div[@class="QEsAKd"]';
    const FINANCIAL_MARKET_OHLC_XPATH = '//div[@class="zz63rd"]';

    /**
     * @var HttpBrowser
     */
    private $browser;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var \NumberFormatter
     */
    private $numberFormatter;

    /**
     * @var FinancialMarketRepository
     */
    private $financialMarketRepository;

    /**
     * @var FinancialMarketPriceRepository
     */
    private $financialMarketPriceRepository;

    /**
     * @var FinancialMarketOHLCRepository
     */
    private $financialMarketOHLCRepository;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param HttpBrowser                       $browser
     * @param RequestStack                      $requestStack
     * @param FinancialMarketRepository         $financialMarketRepository
     * @param FinancialMarketPriceRepository    $financialMarketPriceRepository
     * @param FinancialMarketOHLCRepository     $financialMarketOHLCRepository
     * @param ContainerInterface                $container
     * @param TranslatorInterface               $translator
     * @param LoggerInterface                   $logger
     */
    public function __construct(HttpBrowser $browser, RequestStack $requestStack, FinancialMarketRepository $financialMarketRepository, FinancialMarketPriceRepository $financialMarketPriceRepository, FinancialMarketOHLCRepository $financialMarketOHLCRepository, ContainerInterface $container, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->browser = $browser;
        $this->locale = $requestStack->getCurrentRequest()
            ? $requestStack->getCurrentRequest()->getLocale() : $container->getParameter('locale');
        $this->numberFormatter = new \NumberFormatter($this->locale, \NumberFormatter::DECIMAL);

        $this->financialMarketRepository = $financialMarketRepository;
        $this->financialMarketPriceRepository = $financialMarketPriceRepository;
        $this->financialMarketOHLCRepository = $financialMarketOHLCRepository;

        $this->translator = $translator;
        $this->translator->setLocale($this->locale);

        $this->logger = $logger;
    }

    /**
     * @return FinancialMarket[]
     */
    public function fetchWorldMarkets(): array
    {
        $this->logger->info('Start fetching world markets from Google');

        $crawler = new Crawler();
        $crawler->addHtmlContent($this->browser->fetch('https://finance.google.com/finance'));

        // Extract market data for each row in the table.
        $markets = [];
        $crawler->filterXPath(self::WORLD_MARKETS_XPATH)->each(function (Crawler $node) use (&$markets) {
            $link = $node->filter('g-link');
            $name = $link->text();
            $link = 'https://google.com'.$node->selectLink($name)->attr('href');
            $price = $this->numberFormatter->parse($node->filter('span.IsqQVc')->html());
            $change = $this->numberFormatter->parse(
                str_replace(
                    ['(', ')'],
                    [''],
                    $node->filterXPath('//span[contains(@class, "-Y5HEJA6AmUY")]')->html()
                )
            );
            $shortcut = explode(
                ':',
                $node->filter('div.knowledge-finance-wholepage__additional-info span')->first()->html()
            )[0];

            $financialMarket = $this->financialMarketRepository->createOrUpdate($name, $shortcut, $link);
            $financialMarket->addPrice($this->financialMarketPriceRepository->create($financialMarket, $price, $change));

            $this->financialMarketRepository->save($financialMarket, true);

            $markets[] = $financialMarket;
        });

        $marketNames = array_map(function (FinancialMarket $market) {
            return (string) $market;
        }, $markets);

        $this->logger->info(
            sprintf('Fetched %d markets: %s', count($markets), implode(', ', $marketNames))
        );

        return $markets;
    }

    /**
     * @param FinancialMarket $market
     *
     * @return FinancialMarketOHLC
     *
     * @throws \Exception
     */
    public function fetchOHLC(FinancialMarket $market): FinancialMarketOHLC
    {
        $this->logger->info('Start fetching OHLC values for '.$market->getName());

        $crawler = new Crawler();
        $crawler->addHtmlContent($this->browser->fetch($market->getDetailsUrl()));
        $crawler = $crawler->filterXPath(self::FINANCIAL_MARKET_OHLC_XPATH);

        // Extract financial market OHLC values.
        $marketValues = [];
        $ohlcLabelsTranslated = array_map(function (string $suffix) {
            return $this->translator->trans('finance.google.ohlc.'.$suffix);
        }, ['open', 'high', 'low', 'close']);

        foreach ($ohlcLabelsTranslated as $label) {
            $xPathQuery = sprintf('//td[contains(text(),"%s")]/../td[@class="iyjjgb"]', $label);

            /** @var Crawler $node */
            $node = $crawler->filterXPath($xPathQuery);

            if ($node->count()) {
                $marketValues[] = $this->numberFormatter->parse($node->text());
            }
        }

        // If we haven't close price, we will add it from $market current price.
        if (count($marketValues) === 3) {
            $marketValues[] = $market->getLastPrice()->getPrice();
        }
        if (count($marketValues) !== 4) {
            $this->logger->error(
                sprintf(
                    'Fetching failed: we\'ve got only %d values: %s',
                    count($marketValues),
                    implode(', ', $marketValues)
                )
            );

            throw new \Exception('WHY WHY WHY');
        }
        list($open, $high, $low, $close) = $marketValues;

        // Create and save FinancialMarketOHLC entity.
        $financialMarketOHLC = $this->financialMarketOHLCRepository->create($market, $open, $high, $low, $close);
        $this->financialMarketOHLCRepository->save($financialMarketOHLC, true);

        $this->logger->info(
            sprintf('Fetching was successfully finished: O:%.2f H:%.2f L:%.2f C:%.2f', $open, $high, $low, $close)
        );

        return $financialMarketOHLC;
    }
}
