<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AbstractEntityRepository extends EntityRepository
{
    /**
     * @param object    $entity
     * @param bool      $withFlush
     */
    public function save(object $entity, bool $withFlush = false)
    {
        $this->_em->persist($entity);

        if ($withFlush) {
            $this->_em->flush($entity);
        }
    }
}
