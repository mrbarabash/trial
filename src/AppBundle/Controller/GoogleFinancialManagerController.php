<?php

namespace AppBundle\Controller;

use AppBundle\Manager\GoogleFinanceManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Symfony\Component\Routing\Annotation\Route;

class GoogleFinancialManagerController extends BaseController
{
    /**
     * @Route("/financial/fetch_markets", name="financial_fetch_markets")
     */
    public function fetchWorldMarkets(Request $request, GoogleFinanceManager $manager)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotImplementedException();
        }

        $markets = $manager->fetchWorldMarkets();

        return new JsonResponse(['success' => true, 'count' => count($markets)]);
    }

    /**
     * @Route("/financial/fetch_markets_ohlc", name="financial_fetch_markets_ohlc")
     */
    public function fetchAllOHLC(Request $request, GoogleFinanceManager $manager)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotImplementedException();
        }

        $markets = $manager->fetchWorldMarkets();
        foreach ($markets as $market) {
            $manager->fetchOHLC($market);
        }

        return new JsonResponse(['success' => true]);
    }
}
