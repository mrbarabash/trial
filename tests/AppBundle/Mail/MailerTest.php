<?php

namespace AppBundle\Tests;

use Tests\AppBundle\ControllerTestCase;

class MailerTest extends ControllerTestCase
{
    public function testBestMarketsMail()
    {
        $this->client->enableProfiler();

        $this->client->request('POST', '/mail/best_markets');

        $mailCollector = $this->client->getProfile()->getCollector('swiftmailer');

        static::assertEquals(1, $mailCollector->getMessageCount());

        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];

        static::assertInstanceOf('Swift_Message', $message);
        static::assertEquals($this->getContainer()->get('translator')->trans('emails.best_markets.subject'), $message->getSubject());
        static::assertEquals($this->getContainer()->getParameter('email_from'), key($message->getFrom()));
        static::assertEquals('eyyub@learn-solve.com', key($message->getTo()));
        static::assertNotNull($message->getBody());
    }
}
